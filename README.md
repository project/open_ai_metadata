# Open AI Metadata

## CONTENTS OF THIS FILE

  - Introduction
  - Installation
  - Requirements
  - Configuration
  - Usage
  - Maintainers

## INTRODUCTION

This module uses Open AI and generates meta description for
the different content types.
The meta description generated is based on the title of each content.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/extending-drupal/installing-modules for 
   further information.


## REQUIREMENTS

You need to have an OpenAI API access token.
In order to create a OpenAI access token,
1. Signup onto the Open AI platform
2. Create the API access token at https://platform.openai.com/account/api-keys

## CONFIGURATION

1. Enable the Open AI Metadata module
2. Navigate to /admin/config/open-ai-metadata-settings
3. Configure the Open AI settings
    a. API Endpoint: Enter the Open AI API Endpoint (GPT3.5).
    b. Open AI Access Token: Enter your Open AI access token.
    c. Open AI API Model Name: Enter the API model name, such as 
    'text-davinci-003' (for GPT-3) or 'gpt-3.5-turbo' (for GPT-3.5).
    d. Open AI API Max Token: Enter a max token value to add a word limit 
    in your output.
    e. Open AI Temperature: Enter the temperature value to set the randomness 
    of the output.
4. Navigate to /admin/config/metadata-content-settings
5. Configure the Open AI Metadata Content Settings
    a. Here you can select the content type(s) where in 
    you want to use the feature provided by this module


## USAGE

Configure the Open AI settings and Content type settings
A "Generate Metadata" button will appear on all the node page(s) of the 
selected content types.
Enter the title of the node and click on the "Generate Metadata" button to 
generate the meta description for the node page.
Based on the title provided, Open AI will generate a meta description for 
the node page and will be replaced by the Summary field.
The generated description will then be used as meta description on 
the search engines

## MAINTAINERS

- Vinay Mahale - [vinaymahale](https://www.drupal.org/u/vinaymahale)
