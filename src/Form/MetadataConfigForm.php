<?php

namespace Drupal\open_ai_metadata\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Stores metadata settings.
 */
class MetadataConfigForm extends ConfigFormBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'open_ai_metadata.settings',
    ];
  }

  /**
   * Constructs a MetadataConfigForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'open_ai_metadata_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('open_ai_metadata.settings');
    $state_config = $this->state;

    $form = [
      '#attributes' => ['enctype' => 'multipart/form-data'],
    ];

    $form['openai_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#description' => $this->t('Please provide the OpenAI API Endpoint here.'),
      '#default_value' => $config->get('open_ai_metadata.openai_endpoint') ? $config->get('open_ai_metadata.openai_endpoint') : 'https://api.openai.com/v1/chat/completions',
      '#required' => TRUE,
    ];

    $form['openai_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Access Token'),
      '#description' => $this->t('Please provide the OpenAI Access Token here.'),
      '#default_value' => $state_config->get('open_ai_metadata.openai_token'),
      '#required' => TRUE,
    ];

    $form['openai_model'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Model Name'),
      '#description' => $this->t('Please provide the OpenAI API Model name here.'),
      '#default_value' => $config->get('open_ai_metadata.openai_model'),
      '#required' => TRUE,
    ];

    $form['openai_max_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Max Token'),
      '#description' => $this->t('Please provide the Max token here to limit the output words. Max token is the 
                          limit <br>of tokens combining both input prompt and output text.'),
      '#default_value' => $config->get('open_ai_metadata.openai_max_token'),
      '#required' => TRUE,
    ];

    $form['openai_temperature'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Temperature'),
      '#description' => $this->t('Please provide the OpenAI Temperature value here.'),
      '#default_value' => $config->get('open_ai_metadata.openai_temperature'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $access_token = $form_state->getValue('openai_token');
    $api_max_token = $form_state->getValue('openai_max_token');
    $openai_temperature = $form_state->getValue('openai_temperature');

    if (!empty($access_token) && !preg_match('/^[A-Za-z0-9-_]+$/', $access_token)) {
      $form_state->setErrorByName('openai_token', $this->t('Access Token contains invalid characters. Only alphanumeric characters, hyphens, and underscores are allowed.'));
    }

    if (!empty($api_max_token) && !is_numeric($api_max_token)) {
      $form_state->setErrorByName('openai_max_token', $this->t('OpenAI API Max Token must be a number.'));
    }

    if (!empty($openai_temperature) && !is_numeric($openai_temperature)) {
      $form_state->setErrorByName('openai_temperature', $this->t('OpenAI Temperature must be a number.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $state_config = $this->state;
    $values = $form_state->getValues();
    $state_config->set('open_ai_metadata.openai_token', $values['openai_token']);

    $this->config('open_ai_metadata.settings')
      ->set('open_ai_metadata.openai_endpoint', $values['openai_endpoint'])
      ->set('open_ai_metadata.openai_model', $values['openai_model'])
      ->set('open_ai_metadata.openai_max_token', $values['openai_max_token'])
      ->set('open_ai_metadata.openai_temperature', $values['openai_temperature'])
      ->save();

  }

}
